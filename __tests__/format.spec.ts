import { formatCoordinates } from '../src/format'
import { COORDINATE_FORMATS } from '../src/constants'

const latLon = {
  latitude: 23.53,
  longitude: 43.25
}

describe('Formatting functions:', () => {
  describe('formatCoordinates', () => {
    test('it should return a string in DD if format set to DD', () => {
      expect(
        formatCoordinates(latLon, { format: COORDINATE_FORMATS.DD })
      ).toEqual('23.53°, 43.25°')
    })
    test('it should return a string in DM if format set to DM', () => {
      expect(
        formatCoordinates(latLon, { format: COORDINATE_FORMATS.DM })
      ).toEqual(`23° 31.8', 43° 15'`)
    })
    test('it should return a string in DMS if format set to DMS', () => {
      expect(
        formatCoordinates(latLon, { format: COORDINATE_FORMATS.DMS })
      ).toEqual(`23° 31' 48", 43° 15' 0"`)
    })
  })
})
