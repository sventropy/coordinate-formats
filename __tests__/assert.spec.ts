import {
  assertFloat,
  assertNegativeCoordinate,
  assertTruthy,
  assertOne,
  assertOneOrNone,
  assertSome,
  assertNotNull
} from '../src/assert'
import { SOUTH, NORTH } from '../src/constants'

describe('Assertion functions:', () => {
  describe('assertFloat', () => {
    test('it should return true for a float', () => {
      expect(assertFloat(23.57)).toEqual(true)
    })
    test('it should return false for an integer', () => {
      expect(assertFloat(23)).toEqual(false)
    })
  })

  describe('assertNegativeCoordinate', () => {
    test('it should return true if affix is negative', () => {
      expect(assertNegativeCoordinate(SOUTH)).toEqual(true)
    })
    test('it should return false if affix is positive', () => {
      expect(assertNegativeCoordinate(NORTH)).toEqual(false)
    })
  })

  describe('assertTruthy', () => {
    test('it should return true for true value', () => {
      expect(assertTruthy(true)).toBe(true)
    })
    test('it should return true for truthy values', () => {
      const truthyValues = [' ', 1, [], {}]
      truthyValues.forEach(truthyValue => {
        expect(assertTruthy(truthyValue)).toBe(true)
      })
    })
    test('it should return false for false value', () => {
      expect(assertTruthy(false)).toBe(false)
    })
    test('it should return false for falsy values', () => {
      const falsyValues = ['', 0, undefined, null]
      falsyValues.forEach(falsyValue => {
        expect(assertTruthy(falsyValue)).toBe(false)
      })
    })
  })

  describe('assertOne', () => {
    test('it should return true if only one argument meets the criteria', () => {
      expect(assertOne(assertFloat, 123, 23.45, 23)).toEqual(true)
    })
    test('it should return false if no argument meets the criteria', () => {
      expect(assertOne(assertFloat, 123, 23)).toEqual(false)
    })
    test('it should return false if more than one arguments meets the criteria', () => {
      expect(assertOne(assertFloat, 123.15, 23.23)).toEqual(false)
    })
  })

  describe('assertOneOrNone', () => {
    test('it should return true if only one argument meets the criteria', () => {
      expect(assertOneOrNone(assertFloat, 123, 23.45, 23)).toEqual(true)
    })
    test('it should return true if no argument meets the criteria', () => {
      expect(assertOneOrNone(assertFloat, 123, 23)).toEqual(true)
    })
    test('it should return false if more than one arguments meets the criteria', () => {
      expect(assertOneOrNone(assertFloat, 123.15, 23.23)).toEqual(false)
    })
  })

  describe('assertSome', () => {
    test('it should return true if only one argument meets the criteria', () => {
      expect(assertSome(assertFloat, 123, 23.45, 23)).toEqual(true)
    })
    test('it should return false if no argument meets the criteria', () => {
      expect(assertSome(assertFloat, 123, 23)).toEqual(false)
    })
    test('it should return true if more than one arguments meets the criteria', () => {
      expect(assertSome(assertFloat, 123.15, 23.23)).toEqual(true)
    })
  })

  describe('assertNotNull', () => {
    test('it should return true if argument is not null', () => {
      expect(assertNotNull(123)).toEqual(true)
    })
    test('it should return false if argument is null', () => {
      expect(assertNotNull(null)).toEqual(false)
    })
  })
})
