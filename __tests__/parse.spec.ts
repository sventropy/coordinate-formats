import { parseCoordinates } from '../src/parse'
import { COORDINATE_FORMATS } from '../src/constants'

describe('Parsing functions:', () => {
  describe('parseCoordinates', () => {
    test('it throws an error for a non-parsable string', () => {
      expect(() => {
        parseCoordinates('xcvji')
      }).toThrowError()
    })
    test('it returns a validation error for a string with mixed formats', () => {
      const parsed = parseCoordinates(`23° 31.8', 43.15°`)
      expect(parsed.errors).not.toBeNull()
      expect(parsed.errors!.length).toBeGreaterThanOrEqual(1)
    })
    test('it returns the correct latlon and format for a string in DD', () => {
      const parsed = parseCoordinates(`23.53°, 43.25°`)
      expect(parsed.errors).toBeNull()
      expect(parsed.data).not.toBeNull()
      expect(parsed.data!).toMatchObject({
        latLon: {
          latitude: 23.53,
          longitude: 43.25
        },
        format: COORDINATE_FORMATS.DD
      })
    })
    test('it returns the correct latlon and format for a string in DM', () => {
      const parsed = parseCoordinates(`23° 31.8', 43° 15'`)
      expect(parsed.errors).toBeNull()
      expect(parsed.data).not.toBeNull()
      expect(parsed.data!).toMatchObject({
        latLon: {
          latitude: 23.53,
          longitude: 43.25
        },
        format: COORDINATE_FORMATS.DM
      })
    })
    test('it returns the correct latlon and format for a string in DMS', () => {
      const parsed = parseCoordinates(`23° 31' 48", 43° 15' 0"`)
      expect(parsed.errors).toBeNull()
      expect(parsed.data).not.toBeNull()
      expect(parsed.data!).toMatchObject({
        latLon: {
          latitude: 23.53,
          longitude: 43.25
        },
        format: COORDINATE_FORMATS.DMS
      })
    })
  })
})
