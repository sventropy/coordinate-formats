import {
  dmToDecimal,
  dmsToDecimal,
  decimalToDm,
  decimalToDms
} from '../src/convert'

const DECIMAL = 54.269619
const DM = {
  degrees: 54,
  minutes: 16.1771
}
const DMS = {
  degrees: 54,
  minutes: 16,
  seconds: 10.628
}

describe('Conversion functions:', () => {
  describe('DM to decimal', () => {
    test('it should convert DM to a decimal', () => {
      expect(dmToDecimal(DM)).toBeCloseTo(DECIMAL)
    })
  })

  describe('DMS to decimal', () => {
    test('it should convert DMS to a decimal', () => {
      expect(dmsToDecimal(DMS)).toBeCloseTo(DECIMAL)
    })
  })

  describe('Decimal to DM', () => {
    test('it should convert a decimal to DM', () => {
      const dm = decimalToDm(DECIMAL)
      expect(dm.degrees).toEqual(DM.degrees)
      expect(dm.minutes).toBeCloseTo(DM.minutes)
    })
  })

  describe('Decimal to DMS', () => {
    test('it should convert a decimal to DMS', () => {
      const dms = decimalToDms(DECIMAL)
      expect(dms.degrees).toEqual(DMS.degrees)
      expect(dms.minutes).toEqual(DMS.minutes)
      expect(dms.seconds).toBeCloseTo(DMS.seconds)
    })
  })
})
