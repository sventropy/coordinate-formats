const path = require('path')

module.exports = {
  entry: {
    index: './src/index.ts'
  },
  output: {
    filename: 'coordinate-formats.js',
    library: 'CoordinateFormats',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, './dist')
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(j|t)s$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      },
      {
        test: /\.ts$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'ts-loader'
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  }
}
