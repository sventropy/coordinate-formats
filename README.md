# Coordinate Formats

A library for parsing and formatting coordinates.
https://frond.gitlab.io/coordinate-formats/

![coverage](https://gitlab.com/frond/coordinate-formats/badges/master/coverage.svg?job=test)

## Getting started
```bash
npm i coordinate-formats
```

## Development
Run typescript and webpack in watch mode (with devserver at http://localhost:3000):
```bash
npm start
```

## Testing
Run tests in watch mode:
```
npm run test:watch
```

