import { SYMBOLS_NEGATIVE } from './constants'

/**
 * Assert whether a number is a float.
 *
 * @param number Number to check
 * @return Whether the number is a float.
 */
export const assertFloat = function(number: number): boolean {
  return number % 1 !== 0
}

/**
 * Assert whether the coordinate is negative by checking it's affixes.
 *
 * @param affix The coordinate's prefix.
 * @return Whether the coordinate is negative.
 */
export const assertNegativeCoordinate = function(affix: string): boolean {
  return SYMBOLS_NEGATIVE.includes(affix)
}

export const assertTruthy = function(x: any): boolean {
  return !!x
}

export const assertOne = function(
  f: (x: any) => boolean,
  ...args: any[]
): boolean {
  return args.filter(f).length === 1
}

export const assertOneOrNone = function(
  f: (x: any) => boolean,
  ...args: any[]
): boolean {
  return args.filter(f).length <= 1
}

export const assertSome = function(
  f: (x: any) => boolean,
  ...args: any[]
): boolean {
  return args.some(f)
}

// https://stackoverflow.com/questions/43118692/typescript-filter-out-nulls-from-an-array#46700791
export const assertNotNull = function<T>(x: T | null): x is T {
  return x !== null
}
