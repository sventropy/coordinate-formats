export interface CoordinateSegments {
  prefix: string | null
  degrees: number
  minutes: number | null
  seconds: number | null
  suffix: string | null
}

export interface QuerySegments {
  latitude: CoordinateSegments
  longitude: CoordinateSegments
}

export interface ArcAndDecimalArc {
  arc: number
  decimalArc: number
}

export interface DD {
  degrees: number
}

export interface DM extends DD {
  minutes: number
}

export interface DMS extends DM {
  seconds: number
}

export interface Validation {
  key: string
  template: string
  condition?: Validator
  validator: Validator
}

export type ValidationError = {
  scope: string
  key: string
  message: string
}
export type ValidationErrors = ValidationError[] | null

export type Validator = (...args: any[]) => boolean
export enum CoordinatesFormat {
  DD = 'DD',
  DM = 'DM',
  DMS = 'DMS',
}

export interface LatLon {
  latitude: number
  longitude: number
}

export type Dictionary<T> = { [key: string]: T }

export interface ParsingData {
  latLon: LatLon
  format: CoordinatesFormat
}

export interface ParsingResponse {
  data: ParsingData | null
  errors: ValidationErrors | null
  meta: {
    query: string
    querySegments: QuerySegments
  }
}

export interface FormatOptions {
  format: CoordinatesFormat
  roundTo?: number | null
  useCardinalDirections?: boolean
}

export interface FormatOptionsWithDefaults {
  format: CoordinatesFormat
  roundTo: number | null
  useCardinalDirections: boolean
}

export interface CardinalDirections {
  positive: string
  negative: string
}
