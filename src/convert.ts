import { ArcAndDecimalArc, DM, DMS } from './types'

/**
 * Convert a coordinate in DM (Degrees Minutes) to a decimal.
 *
 * @param dm Coordinate in DM (Degrees Minutes).
 * @returns The equivalent decimal number.
 */
export function dmToDecimal({ degrees, minutes }: DM): number {
  return arcAndDecimalArcToDecimal({ arc: degrees, decimalArc: minutes })
}

/**
 * Convert a coordinate in DMS (Degrees Minutes Seconds) to a decimal.
 *
 * @param dms Coordinate in DMS (Degrees Minutes Seconds).
 * @returns The equivalent decimal number.
 */
export function dmsToDecimal({ degrees, minutes, seconds }: DMS): number {
  return arcAndDecimalArcToDecimal({
    arc: degrees,
    decimalArc: arcAndDecimalArcToDecimal({
      arc: minutes,
      decimalArc: seconds,
    }),
  })
}

/**
 * Convert a decimal to DM (Degrees Minutes).
 *
 * @param decimal The decimal to be converted.
 * @returns The equivalent in DM (Degrees Minutes).
 */
export function decimalToDm(decimal: number): DM {
  return {
    degrees: floatToInteger(decimal),
    minutes: decimalToDecimalMinutes(decimal),
  }
}

/**
 * Convert a decimal to DMS (Degrees Minutes Seconds).
 *
 * @param decimal The decimal to be converted.
 * @returns The equivalent in DMS (Degrees Minutes Seconds).
 */
export function decimalToDms(decimal: number): DMS {
  return {
    degrees: floatToInteger(decimal),
    minutes: floatToInteger(decimalToDecimalMinutes(decimal)),
    seconds: decimalToDecimalSeconds(decimal),
  }
}

/**
 * Convert a arc and decimal arc structure to a decimal number.
 *
 * @param arcAndDecimalArc Arc and decimal arc structure.
 * @returns The equivalent decimal number.
 */
function arcAndDecimalArcToDecimal({
  arc,
  decimalArc,
}: ArcAndDecimalArc): number {
  return arc + decimalArcToDecimalFraction(decimalArc)
}

/**
 * Converts decimal arc units to a decimal fraction.
 *
 * @param decimalArc The decimal arc units to convert.
 * @returns The equivalent decimal fraction.
 */
function decimalArcToDecimalFraction(decimalArc: number): number {
  return decimalArc / 60
}

/**
 * Convert a float to an integer.
 *
 * @param float The float to convert.
 * @returns The resulting integer.
 */
function floatToInteger(float: number): number {
  return ~~float
}

/**
 * Convert a decimal to decimal minutes.
 *
 * @param decimal The decimal to convert.
 * @returns The equivalent in decimal minutes.
 */
function decimalToDecimalMinutes(decimal: number): number {
  return decimalToDecimalArc(decimal)
}

/**
 * Extract the decimal arc units from a decimal.
 *
 * @param decimal The decimal to get the decimal arc untis from.
 * @returns The equivalent decimal arc (minutes or seconds).
 */
function decimalToDecimalArc(decimal: number): number {
  return decimalFractionToDecimalArc(decimalToDecimalFractions(decimal))
}

/**
 * Extract the fractions of a decimal.
 *
 * @param decimal The decimal to extract the fractions from.
 * @returns The extracted fractions.
 */
function decimalToDecimalFractions(decimal: number): number {
  // Instead of dealing with the nasty math issues, let's do it the
  // "JavaScript way" and use a string instead.
  const fractionString = decimal.toString().split('.')[1]
  return parseFloat(`0.${fractionString}`)
}

/**
 * Converts a decimal fraction to decimal arc units.
 *
 * @param decimalFraction The decimal fraction to convert.
 * @returns The equivalent decimal arc (minutes or seconds).
 */
function decimalFractionToDecimalArc(decimalFraction: number): number {
  return decimalFraction * 60
}

/**
 * Convert a decimal to decimal seconds.
 *
 * @param decimal The decimal to convert.
 * @returns The equivalent in decimal seconds.
 */
function decimalToDecimalSeconds(decimal: number): number {
  return decimalToDecimalArc(decimalToDecimalMinutes(decimal))
}
