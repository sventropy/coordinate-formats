import { CoordinatesFormat, CardinalDirections, Dictionary } from './types'

// One regex to match them all
export const REGEX = /([-NS]*)\s?(\d+[.]?\d*\s*(?:deg|Deg|[°d])?)\s*(\d*[.]?\d*\s*(?:min|Min|['’m])*)\s*(\d*[.]?\d*\s*(?:sec|Sec|["s]|'{2}|’{2})*)\s*([-NS]*)[\s,]+([-WE]*)\s?(\d+[.]?\d*\s*(?:deg|Deg|[°d])?)\s*(\d*[.]?\d*\s*(?:min|Min|['’m])*)\s*(\d*[.]?\d*\s*(?:sec|Sec|["s]|'{2}|’{2})*)\s*([-WE]*)/

// Coordinate formats
export const COORDINATE_FORMATS = {
  DD: CoordinatesFormat.DD,
  DM: CoordinatesFormat.DM,
  DMS: CoordinatesFormat.DMS,
}

// Unit symbols
export const DEGREE = '°'
export const MINUTE = "'"
export const SECOND = '"'
export const UNIT_SYMBOLS: Dictionary<string> = {
  degrees: DEGREE,
  minutes: MINUTE,
  seconds: SECOND,
}

// Cardinal direction symbols
export const NORTH = 'N'
export const SOUTH = 'S'
export const EAST = 'E'
export const WEST = 'W'

// Unicode U+002D
export const HYPHEN = '-'

export const CARDINALS_LAT: CardinalDirections = {
  positive: NORTH,
  negative: SOUTH,
}
export const CARDINALS_LON: CardinalDirections = {
  positive: EAST,
  negative: WEST,
}

// Symbols considered as negative in terms of geographic coordinates.
export const SYMBOLS_NEGATIVE = [SOUTH, WEST, HYPHEN]

export const FORMAT_DEFAULTS = {
  format: CoordinatesFormat.DD,
  roundTo: 2,
  useCardinalDirections: false,
}

export const SCOPE_STRINGS: Dictionary<string> = {
  latitude: 'Latitude',
  longitude: 'Longitude',
  latLon: 'Latitude and longitude',
}
