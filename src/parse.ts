import { REGEX, COORDINATE_FORMATS } from './constants'
import {
  QuerySegments,
  CoordinateSegments,
  ParsingData,
  CoordinatesFormat,
  ParsingResponse,
} from './types'
import { validate } from './validate'
import { assertNegativeCoordinate, assertNotNull } from './assert'
import { dmsToDecimal, dmToDecimal } from './convert'

/**
 * Parse coordinates from a string.
 *
 * @param query The string to be parsed.
 * @returns The parsed data (+ errors & meta information).
 */
export function parseCoordinates(query: string): ParsingResponse {
  const match = REGEX.exec(query)
  // TODO Make regex more permissive and handle validation in validate
  // TODO Add warnings too (e.g.: Not the same format for latitude and longitude)!
  if (!match) throw new Error('String could not be parsed.')

  // Get nicely normalized and seperated query parts
  const querySegments = createQuerySegments(match)

  // Validate the parsed query parts
  const errors = validate(querySegments)

  // If there haven't been any errors actually convert it to our internal format
  const data = errors ? null : dataFromQuerySegments(querySegments)

  return {
    data,
    errors,
    meta: {
      query,
      querySegments,
    },
  }
}

/**
 * Create query segments from the regex match.
 *
 * @param match The regex match.
 * @return The query segments.
 */
function createQuerySegments(match: string[]): QuerySegments {
  return {
    latitude: createCoordinateSegments(match.slice(1, 6)),
    longitude: createCoordinateSegments(match.slice(6, 12)),
  }
}

/**
 * Create coordinate segments from the regex match parts.
 *
 * @param matchParts The regex match parts.
 * @return The coordinate segments.
 */
function createCoordinateSegments(matchParts: string[]): CoordinateSegments {
  return {
    prefix: normalizeCoordinateSegment(matchParts[0]),
    degrees: normalizeCoordinateSegment(matchParts[1], {
      isNumber: true,
      isNullable: false,
    }),
    minutes: normalizeCoordinateSegment(matchParts[2], { isNumber: true }),
    seconds: normalizeCoordinateSegment(matchParts[3], { isNumber: true }),
    suffix: normalizeCoordinateSegment(matchParts[4]),
  }
}

/**
 * Normalize a coordinate segment.
 *
 * @param segment The segment to be normalized.
 * @param options The options for this segment.
 * @param options.isNumber Whether the segment is actually a number.
 * @param options.isNullable Whether the segment is nullable.
 * @returns The normalized segment.
 */
function normalizeCoordinateSegment(
  segment: string,
  options: { isNumber: true }
): number | null
function normalizeCoordinateSegment(
  segment: string,
  options: { isNumber: true; isNullable: false }
): number
function normalizeCoordinateSegment(segment: string): string | null
function normalizeCoordinateSegment(
  segment: string,
  options?: { isNumber: boolean; isNullable?: boolean }
): string | number | null {
  const normalized =
    options && options.isNumber ? parseFloat(segment) : segment.trim()
  return segment.length ? normalized : null
}

/**
 * Get the coordinates data from the query string segments.
 *
 * This returns both the format and the converted latitude and longitude in decimals
 * for further formatting.
 * @param segments The segments of the query string.
 * @returns The parsed data.
 */
function dataFromQuerySegments(segments: QuerySegments): ParsingData {
  // Get the format after validation.
  const format = formatFromQuerySegments(segments.latitude)

  return {
    latLon: {
      latitude: coordinateSegmentsToDecimal(segments.latitude),
      longitude: coordinateSegmentsToDecimal(segments.longitude),
    },
    format,
  }
}

/**
 * Get the format by checking the coordinate segments.
 *
 * @param segments The coordinate segments.
 * @returns The coordinate format.
 */
function formatFromQuerySegments(
  segments: CoordinateSegments
): CoordinatesFormat {
  return (
    (segments.seconds && COORDINATE_FORMATS.DMS) ||
    (segments.minutes && COORDINATE_FORMATS.DM) ||
    COORDINATE_FORMATS.DD
  )
}

/**
 * Convert coordinate segments to a decimal.
 *
 * @param coordinateSegments The coordinate segments.
 * @returns The equivalent decimal number.
 */
function coordinateSegmentsToDecimal({
  degrees,
  minutes,
  seconds,
  prefix,
  suffix,
}: CoordinateSegments): number {
  let decimal =
    (assertNotNull(seconds) &&
      assertNotNull(minutes) &&
      dmsToDecimal({ degrees, minutes, seconds })) ||
    (assertNotNull(minutes) && dmToDecimal({ degrees, minutes })) ||
    degrees

  const isNegative =
    (assertNotNull(prefix) && assertNegativeCoordinate(prefix)) ||
    (assertNotNull(suffix) && assertNegativeCoordinate(suffix)) ||
    false
  if (isNegative) decimal = decimal * -1

  return decimal
}
