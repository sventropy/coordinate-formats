import { Validation, CoordinateSegments } from './types'
import {
  assertFloat,
  assertTruthy,
  assertNotNull,
  assertOne,
  assertSome,
  assertOneOrNone,
} from './assert'

/**
 * The checks to be run on the query string.
 */
const validationsQuery: Validation[] = [
  {
    key: 'differentFormats',
    template: '%s have different formats. Please use the same for both.',
    validator: (a: CoordinateSegments, b: CoordinateSegments) =>
      assertNotNull(a.minutes) === assertNotNull(b.minutes) &&
      assertNotNull(a.seconds) === assertNotNull(b.seconds),
  },
]

/**
 * The checks to be run on the coordinates.
 */
const validationsCoordinate: Validation[] = [
  {
    key: 'mixedAffixes',
    template: '%s has both a prefix and a suffix.',
    condition: x => assertSome(assertTruthy, x.prefix, x.suffix),
    validator: x => assertOne(assertTruthy, x.prefix, x.suffix),
  },
  {
    key: 'multipleDecimals',
    template: '%s has multiple values with decimal fractions.',
    condition: x => assertSome(assertTruthy, x.degrees, x.minutes, x.seconds),
    validator: x =>
      assertOneOrNone(
        assertTruthy,
        ...[x.degrees, x.minutes, x.seconds]
          .filter(assertNotNull)
          .map(assertFloat)
      ),
  },
  {
    key: 'impossibleMinutes',
    template: '%s has impossible minutes (> 60).',
    condition: x => assertNotNull(x.minutes),
    validator: x => x.minutes! <= 60,
  },
  {
    key: 'impossibleSeconds',
    template: '%s has impossible seconds (> 60).',
    condition: x => assertNotNull(x.seconds),
    validator: x => x.seconds! <= 60,
  },
]

export { validationsQuery, validationsCoordinate }
