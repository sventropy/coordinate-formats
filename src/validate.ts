import {
  QuerySegments,
  ValidationErrors,
  Validator,
  Validation,
  ValidationError,
} from './types'
import { validationsQuery, validationsCoordinate } from './validations'
import { assertNotNull } from './assert'
import { SCOPE_STRINGS } from './constants'

/**
 * Validate the given query string segments.
 *
 * @param segments The query string segments to validate.
 * @returns The produced validation errors.
 */
export function validate(segments: QuerySegments): ValidationErrors {
  const errorMessages = [
    ...runQueryValidations(segments, validationsQuery),
    ...runCoordinateValidations(segments, validationsCoordinate),
  ]
  return errorMessages.length ? errorMessages : null
}

/**
 * Run the given validation checks on the given query string segments.
 *
 * @param segments The query string segments.
 * @param validations The validation checks.
 * @returns Validation error messages for each check, if an error was produced.
 */
function runQueryValidations(
  segments: QuerySegments,
  validations: Validation[]
) {
  return runValidations(validations, Object.values(segments), 'latLon')
}

/**
 * Run the given validation checks on the given coordinate segments.
 *
 * @param segments The coordinate segments.
 * @param validations The validation checks.
 * @returns Validation error messages for each check, if an error was produced.
 */
function runCoordinateValidations(
  segments: QuerySegments,
  validations: Validation[]
) {
  return Object.entries(segments)
    .map(([coordinateKey, coordinateSegments]) => {
      return runValidations(validations, [coordinateSegments], coordinateKey)
    })
    .reduce((a, b) => a.concat(b))
}

/**
 * Run a set of validations on the given arguments.
 * @param validations The validations to execute.
 * @param validatorArgs The validator arguments.
 * @param scope The error message scope.
 * @return Validation error messages for each check, if an error was produced.
 */
function runValidations(
  validations: Validation[],
  validatorArgs: any[],
  scope: string
): ValidationError[] {
  return validations
    .map(({ key, template, condition, validator }) => {
      const isValid = condition
        ? validateIfApplicaple(condition, validator, ...validatorArgs)
        : validator(...validatorArgs)
      return isValid ? null : createErrorMessage(key, template, scope)
    })
    .filter(assertNotNull)
}

/**
 * Test the coordinate segments on the condition and if it applies, validate.
 *
 * @param condition The condition to test.
 * @param validator The validator to run if the condition is met.
 * @param validatorArgs The validator arguments.
 * @returns Whether the validation succeeded.
 */
function validateIfApplicaple(
  condition: Validator,
  validator: Validator,
  ...validatorArgs: any[]
): boolean {
  return condition(...validatorArgs) ? validator(...validatorArgs) : true
}

/**
 * Create a validation error message.
 * @param key The error key.
 * @param template The message template.
 * @param scope The error message scope.
 * @returns The validation error message.
 */
function createErrorMessage(
  key: string,
  template: string,
  scope: string
): ValidationError {
  return {
    scope,
    key,
    message: template.replace(`%s`, SCOPE_STRINGS[scope]),
  }
}
