import {
  LatLon,
  CoordinatesFormat,
  FormatOptionsWithDefaults,
  CardinalDirections,
  FormatOptions,
  DM,
  DMS,
  DD,
} from './types'
import { decimalToDm, decimalToDms } from './convert'
import {
  CARDINALS_LAT,
  CARDINALS_LON,
  UNIT_SYMBOLS,
  FORMAT_DEFAULTS,
} from './constants'

/**
 * Format the coordinates a string with the given options.
 *
 * @param coordinates The LatLon of coordinates to format.
 * @param options The formatting options.
 * @param options.format The desired format (DD, DM or DMS).
 * @param options.roundTo Whether to round and if, the decimal fractions to round to.
 * @param options.useCardinalDirections Whether to use cardinal directions as prefixes.
 * @returns The formatted string.
 */
export function formatCoordinates(
  coordinates: LatLon,
  options: FormatOptions
): string {
  const optionsWithDefaults = Object.assign({}, FORMAT_DEFAULTS, options)
  return (
    formatCoordinate(coordinates.latitude, optionsWithDefaults, CARDINALS_LAT) +
    ', ' +
    formatCoordinate(coordinates.longitude, optionsWithDefaults, CARDINALS_LON)
  )
}

/**
 * Format a coordinate to a string with the given options.
 *
 * @param coordinate The coordinate to be formatted.
 * @param options The formatting options.
 * @param cardinalDirections The cardinal directions for this coordinate.
 * @return The formatted coordinate.
 */
function formatCoordinate(
  coordinate: number,
  { format, useCardinalDirections, roundTo }: FormatOptionsWithDefaults,
  cardinalDirections: CardinalDirections
): string {
  const converted = convertCoordinateToFormat(coordinate, format)
  const hasNegativeDegrees = converted.degrees < 0

  const convertedEntries = Object.entries(converted)
  const stringParts = convertedEntries.map(([key, val], index) => {
    // Make the degrees value positive if we use another prefix.
    if (key === 'degrees' && useCardinalDirections && hasNegativeDegrees) {
      val = Math.abs(val)
    }

    // Round the last number (which is a decimal).
    if (roundTo !== null && index === convertedEntries.length - 1) {
      val = roundToDecimalPlaces(val, roundTo)
    }

    const symbol = UNIT_SYMBOLS[key]

    return `${val}${symbol}`
  })

  // Add the prefix if using cardinal directions.
  if (useCardinalDirections) {
    stringParts.unshift(
      hasNegativeDegrees
        ? cardinalDirections.negative
        : cardinalDirections.positive
    )
  }

  // Return all the parts united.
  return stringParts.join(' ')
}

/**
 * Convert coordinate in decimals to the desired format.
 *
 * @param coordinate The coordinate as a decimal.
 * @param format The desired format.
 * @returns The coordinate in the desired format as a keyvalue object.
 */
function convertCoordinateToFormat(
  coordinate: number,
  format: CoordinatesFormat
): DD | DM | DMS {
  let converted
  switch (format) {
    case CoordinatesFormat.DD: {
      converted = { degrees: coordinate }
      break
    }
    case CoordinatesFormat.DM: {
      converted = decimalToDm(coordinate)
      break
    }
    case CoordinatesFormat.DMS: {
      converted = decimalToDms(coordinate)
      break
    }
  }
  return converted
}

/**
 * Round a number to the given decimal fractions.
 *
 * @param float The number to be rounded.
 * @param roundTo The decimal fractions to round to.
 * @returns The rounded number.
 */
function roundToDecimalPlaces(float: number, roundTo: number) {
  return +float.toFixed(roundTo)
}
