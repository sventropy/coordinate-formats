import Vue from 'vue'
import { BootstrapVue, ToastPlugin } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faCopy,
  faSquare,
  faCheckSquare,
} from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import App from './App.vue'

library.add(faCopy, faSquare, faCheckSquare)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)
Vue.use(ToastPlugin)
new Vue({
  el: '#app',
  components: { App },
})
